import "lazysizes";

import "./lib/send";
import "./lib/gallery";

import "./sites/adm-gallery";
import "./sites/application";
import "./sites/confirm-application";
import "./sites/faq";
import "./sites/gallery";
import "./sites/index";
import "./sites/my-application";
import "./sites/register";
import "./sites/regulations";
import "./sites/shipment";
import "./sites/start";
import "./sites/statistics";
import "./sites/thank-you";
